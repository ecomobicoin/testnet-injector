FROM rust:1.70.0 as build-env
WORKDIR /app
COPY . /app
RUN cargo build --release

FROM fedora:36
COPY --from=build-env /app/target/release/testnet-injector /target/release/
COPY --from=build-env /app/scripts /scripts/
RUN chmod +x /scripts/*.sh
