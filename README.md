# Testnet injector

A CLI tool to inject transactions or behaviors in an EcoMobiCoin blockchain.

## Components

This project heavily use the forked and adapted [ethers-rs](https://gitlab.limos.fr/ecomobicoin/ethers-rs).

### ETL

-   Extract CSV file e.g. `dataset-extract.csv` to `MobilityRecord`
-   Transform it with simulation params (relative time from earliest entry and behavior attributes)
-   Load data in memory with thread scheduled

### Sending Queue

## Usage

### Simulate bx

To simulate Behaviors from a CSV Mobility dataset, run `cargo run simulate-bx-from-csv data/dataset-extract.csv 60`

Options:

-   dataset e.g. CSV file
-   `total_simulation_duration` in seconds (or minutes ?)

Others examples from MyBus' data for 1 hour:
`cargo run simulate-bx-from-csv ../movement-data/public-transport/MF_user_mobility_june-to-august-2023_mulhouse_dataset/anonymized_june-to-august-2023_mulhouse_dataset.csv 3600`
`cargo run simulate-bx-from-csv ../movement-data/public-transport/MF_user_mobility_june-to-november-2023_france_dataset/anonymized_june-to-november-2023_france_dataset.csv 3600`

### Send tx from CSV

To send tx from accounts that got rewarded from behaviors created from a CSV Mobility dataset, run `cargo run simulate-tx-from-csv data/dataset-extract.csv 10`

Options:

-   dataset e.g. CSV file
-   `block_time` in seconds, send a tx every x seconds (avoiding to send all txs at once and in a single block)
-   (optional) `unknown_addr` the tx will be send to an unknown random address
-   (optional) `rpc_url` Node RPC url e.g. http://127.0.0.1:8545
-   (optional) `chain_id` Chain id e.g. 636363

### Send

Send tx or bx

Options:

-   `txs` Number of transactions to send
-   `bxs` Number of behaviors to send
-   (optional) `hd_index` Index of account in HD wallet, allowing to use `user_id` like done in bx simulation from csv
-   (optional) `wait` Waiting period between each tx/bx
-   (optional) `rpc_url` Node RPC url e.g. http://127.0.0.1:8545
-   (optional) `chain_id` Chain id e.g. 636363

### Mine

To mine a block from env vars `MINER_SIGNING_KEY` and `RPC_URL`, use `cargo run mine`

Options:

-   (optional) `rpc_url` Node RPC url e.g. http://127.0.0.1:8545
-   (optional) `chain_id` Chain id e.g. 636363
-   (optional) `desired_vdf_duration` Desired VDF duration = mining duration e.g. 60secs

Example: `cargo run mine http://127.0.0.1:8545' 636363 60`

## Env vars

You can export env vars in the cli like `export RPC_URL="http://ecomobicoin-2.cri.local.isima.fr:10002"; testnet-injector mine`

Or you can use your user's cargo config file in (see https://doc.rust-lang.org/cargo/reference/config.html) or the project one `cargo/config.toml`.
And set the following content to set env vars:

```
[env]
MINER_SIGNING_KEY = ""
MNEMONIC = "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about"

RPC_URL=http://127.0.0.1:8545
CHAIN_ID = "636363"
DATABASE_URL = "./injector.db"
```

You can also specify `--rpc-url` and `--chain-id` as args in command line that will overwrite env vars.

## Ecomobicoin Simulation

> WORK IN PROGRESS

```mermaid
flowchart LR
    subgraph Configuration
    A[CSV Dataset] --> B[Simulation config]
    AA[Nodes & Miner config] --> B
    AAA[Chainspec] --> B
    end
    subgraph S[Simulation]
    subgraph D[Deployments]
    B --> D1[Deploy nodes]
    B --> D21[Deploy txbx CSV injector]
    B --> D22[Deploy txbx injectors cli]
    B --> D23[Deploy miners cli]
    B --> D3[Deploy block explorer]
    end
    D --> E[Run]
    end
    subgraph R[Reporting]
    E --> R1[Report]
    end
```

### Scripts for testnets

Scripts in `./scripts` are useful to test an EcoMobiCoin network.

| Script                           | Scope                                                                                      |
| -------------------------------- | ------------------------------------------------------------------------------------------ |
| `t0-1mine-2sync.sh`              | 1 node mine an empty block every X seconds, the others sync                                |
| `t1-1mine-with-simulation-2sync` | 1 node mine a block with bxs from simulation data every X seconds, the others sync         |
| `t2-n-miners-m-sync.sh`          | 2 nodes in turn mine blocks with bxs from simulation data every X seconds, the others sync |

### Configuration

#### CSV Dataset

**Behaviors** are created from CSV files like the example file `data/dataset-extract.csv`. More data can be found in https://gitlab.limos.fr/ecomobicoin/data/movement-data/-/tree/master/public-transport/MF_user_mobility_june-to-august-2023_mulhouse_dataset?ref_type=heads

**Transactions** could also be created from CSV if accounts are not empty.

#### Chainspec

`.ron` file included in node, see https://gitlab.limos.fr/ecomobicoin/truite/-/tree/dev/src/res/chainspec?ref_type=heads and documentation about it https://docs.ecomobicoin.limos.fr/docs/basics/protocol-specs

#### Nodes & Miner config

**Node configuration** has low to no impact on the simulation but can be useful to customize data path, api or debug options.

**Miner configuration** can be useful for simulations to have different mining strategies or bx/tx inclusion in block.
Please refer to [`MiningStrategy`](https://gitlab.limos.fr/ecomobicoin/truite/-/blob/dev/src/consensus/pob/miner.rs?ref_type=heads#L90)

## Development

Build the project for dev with `cargo build` or release with `cargo build --release`

Run unit tests with `cargo test`

### Auto-Reloading Development Server

`cargo watch -x run`
