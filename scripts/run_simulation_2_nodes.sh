#!/bin/sh
export MINER_SIGNING_KEY=4c0883a69102937d6231471b5dbb6204fe5129617082792ae468d01a3f362318;
export MNEMONIC='before exclude interest push zone jealous boat ice chimney juice young side diagram silent pipe';

# Check if RPC node is available before starting the simulation
MAX_ATTEMPTS=5
DELAY_BETWEEN_ATTEMPS=3
attempt=1

# First sleep for docker compose container orders
sleep "$DELAY_BETWEEN_ATTEMPS"
while true; do
  # test=$(curl --fail --max-time 3 --header 'Content-Type: application/json' --data '{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":83 }' --location $RPC_URL | grep '"result":"0x0"')
  response=$(curl -s -o /dev/null --header 'Content-Type: application/json' --data '{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":83 }' -w "%{http_code}" $RPC_URL)
  if [ "$response" == "200" ]; then
    break
  fi

  echo "RPC node not ready, let's wait $DELAY_BETWEEN_ATTEMPS seconds"
  sleep "$DELAY_BETWEEN_ATTEMPS"

  attempt=$((attempt + 1))
  if [ "$attempt" -gt "$MAX_ATTEMPTS" ]; then
    exit 1
  fi
done

./target/release/testnet-injector simulate-bx-from-csv ./data/anonymized_june-to-november-2023_france_dataset.csv $TOTAL_SIMULATION_TIME &

echo "TOTAL_SIMULATION_TIME = $TOTAL_SIMULATION_TIME and block time = $BLOCK_TIME"
# Mine
for ((i=1; i<=10000000; i++))
do
    echo "MINE $i";
    if ((i % 2 == 0)); then
        export RPC_URL=$RPC_URL_MINER_1; ./target/release/testnet-injector mine
    else
        export RPC_URL=$RPC_URL_MINER_2; ./target/release/testnet-injector mine
    fi

    if [ $i -ne 10000000 ]; then
        sleep $BLOCK_TIME
    fi
done


