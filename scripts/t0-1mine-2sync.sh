#!/bin/sh
export MINER_SIGNING_KEY=4c0883a69102937d6231471b5dbb6204fe5129617082792ae468d01a3f362318;

# MINER_SIGNING_KEY & MNEMONIC env vars are set on workstation.
# 12 sec
BLOCK_TIME=12;

# export RPC_URL=http://ecomobicoin-1.cri.local.isima.fr:8545;
# export CHAIN_ID=6363;
export RPC_URL=http://127.0.0.1:8545;
export CHAIN_ID=636363;

# Mine on eco1 with `--max-block 0`
#
#  rm -rf /home/angraign/.local/share/truite/chaindata/
# ./truite --chain-spec-file clermont-bootnode.ron --no-dns-discovery --cidr 192.168.0.0/16 --rpc-listen-address 0.0.0.0:8545 --max-block 0
# export RPC_URL=http://ecomobicoin-1.cri.local.isima.fr:8545; export CHAIN_ID=6363; ./target/release/testnet-injector mine
# ./truite --chain-spec-file clermont-bootnode.ron --no-dns-discovery --cidr 192.168.0.0/16 --rpc-listen-address 0.0.0.0:8545
#

# Mine
for ((i=1; i<=100; i++))
do
    echo "MINE $i/100";
    ./target/release/testnet-injector mine
    if [ $i -ne 100 ]; then
        sleep $BLOCK_TIME
    fi
done

# Check if eco2 sync properly


