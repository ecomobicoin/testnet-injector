#!/bin/sh
export MINER_SIGNING_KEY=4c0883a69102937d6231471b5dbb6204fe5129617082792ae468d01a3f362318;
export MNEMONIC='before exclude interest push zone jealous boat ice chimney juice young side diagram silent pipe';

# MINER_SIGNING_KEY & MNEMONIC env vars are set on workstation.
export CHAIN_ID=6363;
#  30 mins
TOTAL_SIMULATION_TIME=180;
# 12 sec
BLOCK_TIME=12;

# export RPC_URL=http://ecomobicoin-2.cri.local.isima.fr:10002;
export RPC_URL=http://ecomobicoin-2.cri.local.isima.fr:8545;
export RPC_URL_MINER=http://ecomobicoin-2.cri.local.isima.fr:8545;
# export RPC_URL=http://127.0.0.1:8545;
# export RPC_URL_MINER=http://127.0.0.1:8545;

# Mine on eco1 with `--max-block 0`
# export RPC_URL=http://ecomobicoin-1.cri.local.isima.fr:8545; export CHAIN_ID=6363; ./target/release/testnet-injector mine

# ./target/release/testnet-injector simulate-bx-from-csv ../movement-data/public-transport/MF_user_mobility_june-to-august-2023_mulhouse_dataset/anonymized_june-to-august-2023_mulhouse_dataset.csv $TOTAL_SIMULATION_TIME &
./target/release/testnet-injector simulate-bx-from-csv ./data/dataset-extract.csv $TOTAL_SIMULATION_TIME &

# Mine every 10 secs or number of blocks
for ((i=1; i<=100; i++))
do
    echo "MINE $i/100";
    export RPC_URL=$RPC_URL_MINER; ./target/release/testnet-injector mine
    if [ $i -ne 100 ]; then
        sleep $BLOCK_TIME
    fi
done


