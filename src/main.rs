use ::clap::{Args, Parser, Subcommand};
use ecomobicoin_jsonrpc::types::{Behavior, Block};
use ethereum_types::U64;
use ethers::{
    prelude::{rand::Rng, *},
    signers::coins_bip39::English,
    types::transaction::ecomobicoin_behavior::EcoMobiCoinBehaviorTransactionRequest,
};
use futures::executor::block_on;
use std::{
    process,
    str::FromStr,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use testnet_injector::utils::{miner::get_miner_behavior, mobility_data::*, txbx};
use tracing::{debug, error, info, warn, Level};

#[derive(Parser)]
#[command(author, version)]
#[command(
    about = "testnet-injector - a simple CLI to interact with a EcoMobiCoin testnet",
    long_about = "The testnet-injector can perform all kinds of operations to enable the block construction of EcoMobiCoin blockchain with transactions (tx) and behaviors (bx)."
)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    /// Send txs and/or bxs using default content
    Send(SendParams),
    /// Simulate behaviors from csv dataset and send them to the node
    SimulateBxFromCsv(SimulateBxFromCsv),
    /// Send transactions from derived accounts from csv dataset
    SendTxFromCsv(SendTxFromCsv),
    /// Mine a block
    Mine(MineParams),
}

#[derive(Args)]
struct SendParams {
    /// Number of transactions to send
    txs: u64,
    /// Number of behaviors to send
    bxs: u64,
    // Index of account in HD wallet
    hd_index: Option<u32>,
    // Waiting period between each tx/bx
    wait: Option<u64>,
    // Node RPC URL
    rpc_url: Option<String>,
    // Chain ID
    chain_id: Option<u64>,
}

#[derive(Args)]
struct SimulateBxFromCsv {
    /// Path to CSV file
    file: String,
    /// Total duration of simulation in seconds
    total_simulation_duration: u64,
    // Node RPC URL
    rpc_url: Option<String>,
    // Chain ID
    chain_id: Option<u64>,
}

#[derive(Args)]
struct SendTxFromCsv {
    /// Path to CSV file
    file: String,
    /// Block time in seconds, will send 1 tx every block
    block_time: u64,
    /// Send tx to unknown addresses
    unknown_addr: Option<bool>,
    // Node RPC URL
    rpc_url: Option<String>,
    // Chain ID
    chain_id: Option<u64>,
}

#[derive(Args)]
struct MineParams {
    // Node RPC URL
    rpc_url: Option<String>,
    // Chain ID
    chain_id: Option<u64>,
    /// Desired VDF mean duration in seconds
    desired_vdf_duration: Option<u64>,
}

async fn init() -> (u64, String) {
    // construct a subscriber that prints formatted traces to stdout
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    // use that subscriber to process traces emitted after this point
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default tracing subscriber failed");

    let rpc_url: String =
        std::env::var("RPC_URL").unwrap_or_else(|_| "http://127.0.0.1:8545".into());
    let provider = Provider::<Http>::try_from(rpc_url.clone()).unwrap();

    let chain_id: u64 = std::env::var("CHAIN_ID")
        .unwrap_or_else(|_| String::from("63"))
        .parse::<u64>()
        .unwrap();

    let block_number: U64 = provider.get_block_number().await.unwrap_or_else(|_| {
        panic!(
            "Failed to get block number from {:?}, cannot go further",
            rpc_url
        )
    });
    info!(
        "Connected to chain id: {:?}, rpc: {:?}, latest block number: {:?}.",
        chain_id, rpc_url, block_number
    );

    (chain_id, rpc_url)
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let cli = Cli::parse();

    let (mut chain_id, mut rpc_url) = init().await;

    match &cli.command {
        Some(Commands::Send(params)) => {
            info!("Sending {:?} tx and {:?} bx ...", params.txs, params.bxs);
            // Override env vars if specified in CLI
            if params.rpc_url.clone().is_some() {
                rpc_url = params.rpc_url.clone().unwrap();
            }
            if params.chain_id.clone().is_some() {
                chain_id = params.chain_id.unwrap();
            }

            let max = std::cmp::max(params.txs, params.bxs);

            let hd_index: u32 = if params.hd_index.is_some() {
                params.hd_index.unwrap()
            } else {
                let mut rng = rand::thread_rng();
                rng.gen()
            };

            let mnemonic = std::env::var("MNEMONIC").unwrap_or("abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about".to_string());
            let wallet = MnemonicBuilder::<English>::default()
                .phrase(PathOrString::String(mnemonic.clone()))
                .index(hd_index)
                .unwrap()
                .build()
                .unwrap()
                .with_chain_id(chain_id);

            let provider = Provider::<Http>::try_from(rpc_url.clone()).unwrap();
            let signer = SignerMiddleware::new(provider.clone(), wallet.clone());
            let addr = signer.clone().address();

            let nonce_manager = signer.clone().nonce_manager(addr);

            for i in 0..max {
                //FIXME nonce doesn't update locally
                let curr_nonce = nonce_manager
                    .get_transaction_count(addr, None)
                    .await
                    .unwrap()
                    .as_u64();

                info!("Signer: 0x{:02x}, nonce: {:?}", addr, curr_nonce);

                if i < params.bxs {
                    let now = SystemTime::now()
                        .duration_since(SystemTime::UNIX_EPOCH)
                        .unwrap()
                        .as_secs();
                    let data = Bytes::from_str("0x015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240").unwrap();

                    let bx: EcoMobiCoinBehaviorTransactionRequest =
                        EcoMobiCoinBehaviorTransactionRequest::new(
                            TransactionRequest::new()
                                // .to(signer.address())
                                .value(U256::from(210000))
                                .chain_id(chain_id)
                                .data(data.0.clone()),
                            Some(U64::from(now)),
                        );

                    // bxs don't have their own nonce yet so don't increment it.
                    let bx_hash = signer.send_transaction(bx, None).await;
                    info!("Sent default bx {:?}", bx_hash);
                }
                if i < params.txs {
                    let tx = TransactionRequest::new()
                        .to(Address::random())
                        .value(U256::from(210000))
                        .chain_id(chain_id);
                    let tx_hash = nonce_manager.send_transaction(tx, None).await;
                    info!("Sent default tx {:?}", tx_hash);
                }
                if params.wait.is_some() {
                    tokio::time::sleep(Duration::from_secs(params.wait.unwrap())).await;
                }
            }
        }
        Some(Commands::SimulateBxFromCsv(params)) => {
            info!(
                "Simulating bx from {:?} for a total time of {:?} secs",
                params.file.clone(),
                params.total_simulation_duration.clone()
            );
            // Override env vars if specified in CLI
            if params.rpc_url.clone().is_some() {
                rpc_url = params.rpc_url.clone().unwrap();
            }
            if params.chain_id.clone().is_some() {
                chain_id = params.chain_id.unwrap();
            }
            let records = read_csv(csv::Reader::from_path(params.file.clone())?);
            if let Err(err) = records {
                error!("error running example: {}", err);
                process::exit(1);
            }
            let records = records.unwrap();

            let mobility_records = generate_behaviors_from_records(records.clone());

            let dataset_max_relative_time = mobility_records
                .iter()
                .max_by(|x, y| x.relative_start_time.cmp(&y.relative_start_time))
                .unwrap()
                .relative_start_time;

            let total_simulation_duration = if params.total_simulation_duration > 0 {
                params.total_simulation_duration
            } else {
                dataset_max_relative_time as u64
            };

            info!(
                "Simulation of {:?} records for {:?}secs (max time {:?})",
                mobility_records.len(),
                total_simulation_duration,
                Duration::from_millis(dataset_max_relative_time as u64)
            );

            let mobility_records_times: Vec<Duration> = mobility_records
                .iter()
                .map(|r| {
                    compute_stretched_relative_start_time(
                        r.relative_start_time as u64,
                        total_simulation_duration,
                        dataset_max_relative_time as u64,
                    )
                })
                .collect();

            // ----------------
            // Sending queue
            // ----------------
            let mut handles = Vec::with_capacity(mobility_records.len());

            for i in 0..mobility_records.len() {
                let now = SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .as_secs();
                let simulation_behavior = mobility_records[i].clone();
                let relative_start_duration = if i == 0 {
                    Duration::from_secs(0)
                } else {
                    mobility_records_times[i] - mobility_records_times[i - 1]
                };
                let rpc_url = rpc_url.clone();
                let chain_id = chain_id;
                let mut time_since_beginning = now;
                tokio::time::sleep(relative_start_duration).await;

                handles.push(tokio::spawn({
                    async move {
                            time_since_beginning += relative_start_duration.as_secs();

                            let provider = Provider::<Http>::try_from(rpc_url.clone()).unwrap();

                            debug!("Waited {:?} for behavior {:?}", relative_start_duration, simulation_behavior.id);
                            // Wallet is derived from mnemomic with user_id to ensure each user have the same wallet for the simulation.
                            let mnemonic = std::env::var("MNEMONIC").unwrap_or("abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about".to_string());
                            let wallet = MnemonicBuilder::<English>::default()
                                .phrase(PathOrString::String(mnemonic.clone()))
                                .index(simulation_behavior.mobility_record.user_id as u32)
                                .unwrap()
                                .build()
                                .unwrap()
                                .with_chain_id(chain_id);

                            info!(
                                "Sending bx {:?}, qty: {:?},user {:?} with wallet {:?} ",
                                simulation_behavior.id,
                                simulation_behavior.quantity,
                                simulation_behavior.mobility_record.user_id,
                                wallet.address()
                            );
                            let client = SignerMiddleware::new(provider.clone(), wallet.clone());

                            let bx_hash = txbx::send_behavior_transaction(
                                client.clone(),
                                chain_id,
                                time_since_beginning,
                                simulation_behavior.quantity,
                                simulation_behavior.mobility_record.to_bytes()
                            ).await;
                            info!("Sent bx({:?}): {:?}",simulation_behavior.id,bx_hash);
                    }
                        }))
            }

            info!("Scheduling done, waiting for bx & tx threads...");

            // Wait for all of them to complete.
            for handle in handles {
                block_on(handle).unwrap();
            }
        }
        Some(Commands::SendTxFromCsv(params)) => {
            info!(
                "Sending tx from {:?} every {:?} secs with unknown address={:?}",
                params.file, params.block_time, params.unknown_addr
            );
            // Override env vars if specified in CLI
            if params.rpc_url.clone().is_some() {
                rpc_url = params.rpc_url.clone().unwrap();
            }
            if params.chain_id.clone().is_some() {
                chain_id = params.chain_id.unwrap();
            }
            let records = read_csv(csv::Reader::from_path(params.file.clone())?);
            if let Err(err) = records {
                error!("error running example: {}", err);
                process::exit(1);
            }
            let records = records.unwrap();

            let mobility_records = generate_behaviors_from_records(records.clone());

            let is_addr_unknown = params.unknown_addr.unwrap_or(false);
            let block_time = if params.block_time > 0 {
                params.block_time
            } else {
                12 // Ethereum PoS block time
            };

            info!("Simulation of {:?} records", mobility_records.len());

            // ----------------
            // Sending queue
            // ----------------
            let mut handles = Vec::with_capacity(mobility_records.len());

            for (i, mobility_behavior) in mobility_records.iter().enumerate() {
                let simulation_behavior = mobility_behavior.clone();
                let rpc_url = rpc_url.clone();
                let chain_id = chain_id;
                let sleep_duration = Duration::from_secs(block_time * i as u64);

                handles.push(tokio::spawn({
                    async move {
                            tokio::time::sleep(sleep_duration).await;

                            let provider = Provider::<Http>::try_from(rpc_url.clone()).unwrap();

                            // Wallet is derived from mnemomic with user_id to ensure each user have the same wallet for the simulation.
                            let mnemonic = std::env::var("MNEMONIC").unwrap_or("abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about".to_string());
                            let user_id = simulation_behavior.mobility_record.user_id as u32;
                            let wallet = MnemonicBuilder::<English>::default()
                                .phrase(PathOrString::String(mnemonic.clone()))
                                .index(user_id)
                                .unwrap()
                                .build()
                                .unwrap()
                                .with_chain_id(chain_id);

                            let signer = SignerMiddleware::new(provider.clone(), wallet.clone());
                            let addr = signer.clone().address();
                            let nonce_manager = signer.nonce_manager(addr);

                            // List of active accounts from node
                            let accounts = provider.request::<[Behavior;0], Vec<(Address, U256)>>("ots_getAccountWithBalance", []).await.unwrap();

                            let to_addr = if is_addr_unknown {
                                Address::random()
                            } else {
                                let mut rng = rand::thread_rng();
                                let rnd = rng.gen_range(0..accounts.len());
                                accounts[rnd].0
                            };

                            let from_balance = provider.get_balance(wallet.address(),None).await.unwrap();

                            // Temporary value close to min
                            let tx_value= U256::from(210000);

                            if from_balance.gt(&tx_value) {
                                let tx = TransactionRequest::new()
                                .to(to_addr)
                                .value(tx_value)
                                .chain_id(chain_id);

                                info!(
                                    "Sending tx from: {:?} (user_id: {:?}),to {:?} value {:?} ",
                                    addr,
                                    user_id,
                                    to_addr,
                                    tx_value
                                );

                                let tx_hash = nonce_manager.send_transaction(tx, None).await;
                                info!("Sent tx {:?}", tx_hash);
                            } else {
                                warn!("Didn't send tx for {:?}, balance too low ({:?}<210000)", addr, from_balance);
                            }
                        }
                        }))
            }

            info!("Scheduling done, waiting for tx threads...");

            // Wait for all of them to complete.
            for handle in handles {
                block_on(handle).unwrap();
            }
        }
        Some(Commands::Mine(params)) => {
            // Override env vars if specified in CLI
            if params.rpc_url.clone().is_some() {
                rpc_url = params.rpc_url.clone().unwrap();
            }
            if params.chain_id.clone().is_some() {
                chain_id = params.chain_id.unwrap();
            }

            // Wallet is derived from mnemomic with user_id to ensure each user have the same wallet for the simulation.
            let miner_signing_key =
                std::env::var("MINER_SIGNING_KEY").expect("Must specify a miner private key");
            let wallet: LocalWallet = miner_signing_key.parse::<LocalWallet>().unwrap();
            let miner_addr = wallet.address();
            info!("Mining with 0x{:02x} ...", miner_addr);

            let provider = Provider::<Http>::try_from(rpc_url.clone()).unwrap();
            let network_difficulty = if params.desired_vdf_duration.is_some() {
                let latest_block = provider.get_block(BlockNumber::Latest).await;
                Some(
                    ethnum::U256::from_str(&latest_block.unwrap().unwrap().difficulty.to_string())
                        .unwrap(),
                )
            } else {
                None
            };

            let behavior = get_miner_behavior(
                wallet,
                chain_id,
                miner_addr,
                network_difficulty,
                params.desired_vdf_duration,
            );

            debug!(
                "Miner behavior: {:?}",
                serde_json::to_string(&behavior.clone())
            );
            // SEND IT
            let block = provider
                .request::<[Behavior; 1], Block>("emc_mine", [behavior])
                .await
                .unwrap();

            debug!("Mining response: {:?}", block);
            info!("Mined block {:?}", block.number.unwrap())
        }
        None => {}
    }
    Ok(())
}
