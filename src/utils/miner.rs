use std::time::SystemTime;

use ecomobicoin_jsonrpc::types::{Behavior, BehaviorMessage, Bytes};
use ethereum_interfaces::types::H256;
use ethereum_types::{Address, U64};
use ethers::utils::keccak256;
use ethers::{
    signers::{LocalWallet, Signer},
    types::{
        transaction::ecomobicoin_behavior::EcoMobiCoinBehaviorTransactionRequest,
        TransactionRequest,
    },
};
use ethnum::U256;

/// Get a RPC Behavior for miner with default values, ready to send.
pub fn get_miner_behavior(
    wallet: LocalWallet,
    chain_id: u64,
    to: Address,
    network_difficulty: Option<U256>,
    desired_vdf_duration: Option<u64>,
) -> Behavior {
    let now = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs();

    // const VDF_DIFFICULTY_FOR_10_MIN: u64 = 72;
    // Mean VDF difficulty per sec = 72/600 on current machine (Macbook M1) and a hash maxed to 1 day.
    // for a network difficulty of 1000000
    const MEAN_VDF_DIFFICULTY_PER_SEC: u64 = 12;

    let quantity = match network_difficulty {
        Some(net_dif) => match desired_vdf_duration {
            Some(vdf_dur) => net_dif
                .saturating_mul(U256::from(MEAN_VDF_DIFFICULTY_PER_SEC * vdf_dur))
                .saturating_div(U256::from(1000000_u128)),
            _ => net_dif
                .saturating_mul(U256::from(MEAN_VDF_DIFFICULTY_PER_SEC * 60))
                .saturating_div(U256::from(1000000_u128)),
        },
        //  default
        _ => U256::from(10_000_u128),
    };
    let behavior_msg = BehaviorMessage{
        chain_id: U64::from(chain_id),
        to:Some(to),
        timestamp: U64::from(now),
        quantity,
        input: Bytes::from("0x015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240") };

    // let behavior = wallet.
    let tr = TransactionRequest::new()
        .to(to)
        .value(primitive_types::U256::from(quantity.as_u128()))
        .chain_id(chain_id)
        .data(behavior_msg.input.0.clone());

    let bx = EcoMobiCoinBehaviorTransactionRequest::new(tr, Some(U64::from(now)));
    // wallet.sig
    let signature = wallet
        .sign_transaction_sync(
            &ethers::types::transaction::eip2718::TypedTransaction::EcoMobiCoinBehavior(bx.clone()),
        )
        .unwrap();

    let behavior_rlp = bx.rlp();
    let bx_hash: primitive_types::H256 = keccak256(behavior_rlp.as_ref()).into();
    let hash = primitive_types::H256::from(bx_hash);
    Behavior {
        message: behavior_msg,
        v: signature.v.into(),
        r: H256::from(signature.r).into(),
        s: H256::from(signature.s).into(),
        from: wallet.address(),
        hash,
        behavior_index: None,
        block_number: None,
        block_hash: None,
    }
}
