use csv::Reader;
use ethers::types::Bytes;
use primitive_types::U256;
use serde::{Deserialize, Serialize};
use std::{error::Error, ops::Sub, time::Duration};
use time::OffsetDateTime;

/// Mobility Record from Public Transportation
/// ---
/// Created from data in `data/dataset-extract.csv` and `data/sample-2-extract.csv`
///
/// CSV header:
/// id,user_id,network,service,label,created_at,validation_datetime,from_coord,from_label,from_datetime,to_coord,to_label,to_datetime,from_latitude,from_longitude,to_latitude,to_longitude
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct MobilityRecord {
    pub id: u64,
    pub user_id: u64,
    pub network: String,
    pub service: String,
    pub label: String,
    // `YYYY-MM-DD HH:mm:ss.SSSSSS` rfc3339
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: time::OffsetDateTime,

    /// Departure coordinates in hex PostGIS EWKB (Extended Well-Known Binary)
    pub from_coord: Option<String>,
    pub from_label: Option<String>,
    // either `YYYY-MM-DD HH:mm:ss` or `YYYY-MM-DD HH:mm:ss.SSS` rfc3339
    #[serde(with = "time::serde::rfc3339::option")]
    pub from_datetime: Option<time::OffsetDateTime>,
    pub from_latitude: Option<f64>,
    pub from_longitude: Option<f64>,
    /// Arrival coordinates in hex PostGIS EWKB (Extended Well-Known Binary)
    pub to_coord: Option<String>,
    pub to_label: Option<String>,
    // `YYYY-MM-DD HH:mm:ss` rfc3339
    #[serde(with = "time::serde::rfc3339::option")]
    pub to_datetime: Option<time::OffsetDateTime>,
    pub to_latitude: Option<f64>,
    pub to_longitude: Option<f64>,

    #[serde(with = "time::serde::rfc3339::option")]
    pub validation_datetime: Option<time::OffsetDateTime>,
}

impl MobilityRecord {
    // Convert to a byte array
    // TODO: update serialization when CSV struct is in final version
    pub fn to_bytes(&self) -> Bytes {
        let serialized_mobility_record = serde_json::to_string(self).unwrap();
        let bytes = serialized_mobility_record.as_bytes();
        Bytes::from(bytes.to_vec())
    }
}

pub fn read_csv<R: std::io::Read>(
    mut reader: Reader<R>,
) -> Result<Vec<MobilityRecord>, Box<dyn Error>> {
    let mut records = vec![];
    for result in reader.deserialize() {
        let record: MobilityRecord = result?;
        // println!("{:?}", record);
        records.push(record)
    }
    Ok(records)
}

/// Mobility Behavior
/// ---
///
/// Including a MobilityRecord from CSV extraction
#[derive(Debug, Clone)]
pub struct MobilityBehavior {
    pub id: u64,
    // pub dataset_id: u64,
    // in milliseconds
    pub relative_start_time: i128,
    pub quantity: U256,
    pub mobility_record: MobilityRecord,
}

pub fn find_earliest_mobility_record(records: &[MobilityRecord]) -> OffsetDateTime {
    records
        .iter()
        .min_by(|x, y| x.created_at.cmp(&y.created_at))
        .unwrap()
        .created_at
}

pub fn find_latest_mobility_record(records: &[MobilityRecord]) -> OffsetDateTime {
    records
        .iter()
        .max_by(|x, y| x.created_at.cmp(&y.created_at))
        .unwrap()
        .created_at
}

pub fn generate_behaviors_from_records(records: Vec<MobilityRecord>) -> Vec<MobilityBehavior> {
    let earliest_datetime = find_earliest_mobility_record(&records);

    // let dataset_id = thread_rng().gen::<u64>();
    let mut start_times:Vec<i128> = records.iter().map(|r| r.created_at.sub(earliest_datetime).whole_milliseconds()).collect();
    start_times.sort_unstable();

    records
        .iter()
        .enumerate()
        .map(|(i, r)| MobilityBehavior {
            id: i as u64,
            // dataset_id,
            relative_start_time: start_times[i],
            quantity: quantify_mobility(r.service.clone()),
            mobility_record: r.clone(),
        })
        .collect::<Vec<MobilityBehavior>>()
}

/// Strech behavior start time for simulation
pub fn compute_stretched_relative_start_time(
    relative_start_time: u64,
    total_simulation_duration: u64,
    dataset_max_relative_time: u64,
) -> Duration {
    Duration::from_millis(
        (relative_start_time * total_simulation_duration * 1000) / dataset_max_relative_time,
    )
}

/// Provide differents fake quantities depending on mobility e.g. BIKE that will be use for rewards calculation
/// Coefficients per km from https://nosgestesclimat.fr/actions/plus/transport/voiture-5km
pub fn quantify_mobility(service: String) -> U256 {
    let base_quantity = U256::from(1000000 * 12); // default network difficulty * mean vdf difficulty per sec

    // U256::from(1_000_000_000_000_000_000_000_000_000_u128);
    if service == "BIKE_SHARING" {
        // 0.129/0.0001 = 1290
        base_quantity.saturating_mul(U256::from(1290_u128))
    } else if service == "BIKE_RENTAL" {
        base_quantity.saturating_mul(U256::from(1290_u128))
    } else if service == "PUBLIC_TRANSPORT" {
        base_quantity.saturating_mul(U256::from(1_u128))
    } else {
        base_quantity
    }
}

#[cfg(test)]
mod tests {
    use time::OffsetDateTime;

    use super::*;

    #[test]
    fn read_csv_from_str_should_work() {
        // ARRANGE
        let example_data = "\
id,user_id,network,service,label,created_at,validation_datetime,from_coord,from_label,from_datetime,to_coord,to_label,to_datetime,from_latitude,from_longitude,to_latitude,to_longitude
400,9,A,B,C,2023-07-02T21:02:04.000Z,2023-07-02T21:02:04.000Z,,,,,,,,,,
1337,79,mulhouse,PUBLIC_TRANSPORT,Abo,2023-07-02T19:02:04.000Z,2023-07-02T20:02:04.000Z,DEADC0DE,TEST,2023-07-02T21:02:04.000Z,DEADC0DE,TEST,2023-07-02T21:02:04.000Z,0.0,0.0,0.0,0.0
6620,46583,city-C,PUBLIC_TRANSPORT,Titre Journée,2023-06-02T10:24:20+01:00,2023-06-03T12:55:22+01:00,,,,,,,,,,
";
        let rdr = Reader::from_reader(example_data.as_bytes());
        // ACT
        let result = read_csv(rdr).unwrap();
        // ASSERT
        assert_eq!(3, result.len())
    }

    #[test]
    fn to_bytes_should_work() {
        // ARRANGE
        let mobility_record = MobilityRecord {
            id: 1337,
            user_id: 1,
            network: String::from("mulhouse"),
            service: String::from("PUBLIC_TRANSPORT"),
            label: String::from("BLABLABLA"),
            created_at: OffsetDateTime::from_unix_timestamp(1699353308).unwrap(),
            from_datetime: None,
            from_coord: None,
            from_label: None,
            from_latitude: None,
            from_longitude: None,
            to_datetime: None,
            to_coord: None,
            to_label: None,
            to_latitude: None,
            to_longitude: None,
            validation_datetime: None,
        };

        // ACT
        let result = mobility_record.to_bytes();
        // ASSERT

        // FIXME Binary data change between tests o
        assert!(!result.to_string().is_empty());
        assert_eq!("0x7b226964223a313333372c22757365725f6964223a312c226e6574776f726b223a226d756c686f757365222c2273657276696365223a225055424c49435f5452414e53504f5254222c226c6162656c223a22424c41424c41424c41222c22637265617465645f6174223a22323032332d31312d30375431303a33353a30385a222c2266726f6d5f636f6f7264223a6e756c6c2c2266726f6d5f6c6162656c223a6e756c6c2c2266726f6d5f6461746574696d65223a6e756c6c2c2266726f6d5f6c61746974756465223a6e756c6c2c2266726f6d5f6c6f6e676974756465223a6e756c6c2c22746f5f636f6f7264223a6e756c6c2c22746f5f6c6162656c223a6e756c6c2c22746f5f6461746574696d65223a6e756c6c2c22746f5f6c61746974756465223a6e756c6c2c22746f5f6c6f6e676974756465223a6e756c6c2c2276616c69646174696f6e5f6461746574696d65223a6e756c6c7d",
         result.to_string());
    }

    #[test]
    fn find_earliest_mobility_record_should_work() {
        // ARRANGE
        let rdr = Reader::from_path("./data/dataset-extract.csv").unwrap();
        let records = read_csv(rdr).unwrap();

        // ACT
        let earliest_datetime = find_earliest_mobility_record(&records);

        // ASSERT
        assert_eq!(
            1685570991226773000,
            earliest_datetime.unix_timestamp_nanos()
        );
    }

    #[test]
    fn find_latest_mobility_record_should_work() {
        // ARRANGE
        let rdr = Reader::from_path("./data/dataset-extract.csv").unwrap();
        let records = read_csv(rdr).unwrap();

        // ACT
        let latest_datetime = find_latest_mobility_record(&records);

        // ASSERT
        assert_eq!(1688324524987621000, latest_datetime.unix_timestamp_nanos());
    }

    #[test]
    fn create_behaviors_from_records_should_work() {
        // ARRANGE
        let rdr = Reader::from_path("./data/dataset-extract.csv").unwrap();
        let records = read_csv(rdr).unwrap();

        // ACT
        let behaviors = generate_behaviors_from_records(records.clone());
        // ASSERT
        assert_eq!(records.len(), behaviors.len());
    }

    #[test]
    fn create_behaviors_from_sample2_should_work() {
        // ARRANGE
        let rdr = Reader::from_path("./data/sample-2-extract.csv").unwrap();
        let records = read_csv(rdr).unwrap();

        // ACT
        let behaviors = generate_behaviors_from_records(records.clone());
        // ASSERT
        assert_eq!(records.len(), behaviors.len());
    }

    #[test]
    fn compute_stretched_relative_start_time_should_work_0() {
        // ARRANGE

        // ACT
        let result = compute_stretched_relative_start_time(0, 10, 1000);

        // ASSERT
        assert_eq!(Duration::from_secs(0), result);
    }

    #[test]
    fn compute_stretched_relative_start_time_should_work() {
        // ARRANGE
        // 1000 ms
        let relative_start_time = 1000;
        // 10 secs
        let total_simulation_duration = 10;
        // 2 sec
        let dataset_max_relative_time = 2000;

        // ACT
        let result = compute_stretched_relative_start_time(
            relative_start_time,
            total_simulation_duration,
            dataset_max_relative_time,
        );
        // ASSERT
        assert_eq!(Duration::from_secs(5), result);
    }
    #[test]
    fn compute_stretched_relative_start_time_should_work_max() {
        // ARRANGE
        // 1000 ms
        let relative_start_time = 2000;
        // 10 secs
        let total_simulation_duration = 10;
        // 2 sec
        let dataset_max_relative_time = 2000;

        // ACT
        let result = compute_stretched_relative_start_time(
            relative_start_time,
            total_simulation_duration,
            dataset_max_relative_time,
        );
        // ASSERT
        assert_eq!(Duration::from_secs(10), result);
    }

    #[test]
    fn quantify_mobility_bike() {
        // ARRANGE
        // ACT
        let result = quantify_mobility(String::from("BIKE_SHARING"));
        // ASSERT
        assert_eq!(U256::from(15480000000_u128), result);
    }
}
