use ethereum_types::{Address, U256, U64};
use ethers::{
    prelude::*,
    types::transaction::{
        ecomobicoin_behavior::EcoMobiCoinBehaviorTransactionRequest, eip2718::TypedTransaction,
    },
    utils::keccak256,
};
use std::{str::FromStr, time::SystemTime};
use tracing::debug;

pub async fn send_transaction(
    signer: SignerMiddleware<Provider<Http>, LocalWallet>,
    chain_id: u64,
    to: Address,
    value: U256,
) -> H256 {
    // craft the transaction
    let tx = TransactionRequest::new()
        .to(to)
        .value(value)
        .chain_id(chain_id);

    let pending_tx: PendingTransaction<'_, Http> = signer.send_transaction(tx, None).await.unwrap();
    pending_tx.tx_hash()
}
pub async fn send_behavior_transaction(
    signer: SignerMiddleware<Provider<Http>, LocalWallet>,
    chain_id: u64,
    timestamp: u64,
    quantity: U256,
    data: Bytes,
) -> H256 {
    let tr = TransactionRequest::new()
        // .to(signer.address())
        .value(quantity)
        .chain_id(chain_id)
        .data(data.0.clone());

    let bx = EcoMobiCoinBehaviorTransactionRequest::new(tr, Some(U64::from(timestamp)));
    // let typed_tx = transaction::eip2718::TypedTransaction::EcoMobiCoinBehavior(bx);
    // let bx_signed = signer.sign_transaction(&typed_tx);
    // send it!
    let pending_bx: PendingTransaction<'_, Http> = signer.send_transaction(bx, None).await.unwrap();

    let bx: EcoMobiCoinBehaviorTransactionRequest = EcoMobiCoinBehaviorTransactionRequest::new(
        TransactionRequest::new()
            // .to(signer.address())
            .value(quantity)
            .chain_id(chain_id)
            .data(data.0.clone()),
        Some(U64::from(timestamp)),
    );
    debug!("bx {:?}", bx);

    let bx_sighash = TypedTransaction::EcoMobiCoinBehavior(bx.clone()).sighash();
    debug!("bx_sighash {:02x}", bx_sighash);

    let behavior_rlp = bx.rlp();
    debug!("behavior_rlp {:02x}", behavior_rlp.clone());
    let bx_hash: H256 = keccak256(behavior_rlp.as_ref()).into();
    debug!("bx_hash {:02x}", bx_hash);

    pending_bx.tx_hash()
}

pub async fn send_default_tx(
    signer: SignerMiddleware<Provider<Http>, LocalWallet>,
    chain_id: u64,
) -> H256 {
    send_transaction(signer, chain_id, Address::zero(), U256::from(210000)).await
}
pub async fn send_default_bx(
    signer: SignerMiddleware<Provider<Http>, LocalWallet>,
    chain_id: u64,
) -> H256 {
    let now = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs();
    send_behavior_transaction(signer, chain_id, now,U256::from(210000), Bytes::from_str("0x015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240").unwrap()).await
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn build_default_tx_test() {
        // ARRANGE
        // ACT
        // ASSERT
    }

    #[test]
    fn build_default_bx_test() {
        // ARRANGE
        // ACT
        // ASSERT
    }
}
